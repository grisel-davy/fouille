from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoAlertPresentException
import re

email_regex = pattern=re.compile('([\w_.+-]+@[\w_.+-]+\.[\w_.+-]+)')

class bcolors:
    """ Couleurs pour l'affichage dans le terminal """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def initialize():
    """Initialise le browser et renvoit l'object browser """
    opts = webdriver.FirefoxOptions()
    opts.headless = True
    browser = webdriver.Firefox(options=opts)
    return(browser)

def fouille(site, v=True):
    """ Recherche de toute les adresses (si possible de contacte) sur le site donné 
    Paramètre: site = str, v = boolean (verbosness)
    Sortie: liste des adresses trouvées
    """
    
    addrs = []
    browser = initialize()

    if 'http' not in site:
        site = 'https://'+site

    browser.get(site)

    addrs_main = list(dict.fromkeys(email_regex.findall(browser.page_source)))
    addrs += addrs_main

    if v: 
        if addrs_main != []:
            print(bcolors.OKGREEN + str(addrs_main) + bcolors.ENDC)
        else:
            print(bcolors.OKBLUE + "No address found on main page." + bcolors.ENDC)

    # Finding the contact page
    contact_link = None
    try:
        contact_link = browser.find_element_by_xpath("//a[contains(@text, 'Contact') or contains(@text, 'contact') or contains(@href, 'contact')]")
    except NoSuchElementException:
        print(bcolors.OKBLUE + "No contact page found." + bcolors.ENDC)

    if contact_link != None:
        if v: print("Contact element found:\n{}".format(contact_link.get_attribute('innerHTML')))
        href = contact_link.get_property('href')
        if not 'mailto' in href:
            if v:print("Going to {}".format(href))
            browser.get(href)
            browser.implicitly_wait(0.5)
            addrs_contact = list(dict.fromkeys(email_regex.findall(browser.page_source)))
            addrs += addrs_contact
        else:
            addrs_contact = [href.split(':')[1]]
            addrs += addrs_contact
        
        addrs = list(dict.fromkeys(addrs))
        if v:
            if addrs_contact != []:
                print(bcolors.OKGREEN + str(addrs_contact) + bcolors.ENDC)
            else:
                print(bcolors.OKBLUE + "No address found on contact page." + bcolors.ENDC)

        if v: print("Résumé:\nSite: {}\nNombre d'adresses: {}".format(site, len(addrs)))
    
    return addrs
